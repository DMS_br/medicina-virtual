**ProjectTeamSharedKernel**

This repository contains code to start a fresh VR project for Oculus Quest, which also works for desktops.

It contains the Standard Assets for VR, scripts for reading for inputs for VR, and many scripts for simples VR stuff like buttons.
